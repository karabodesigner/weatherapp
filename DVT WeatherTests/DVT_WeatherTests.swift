//
//  DVT_WeatherTests.swift
//  DVT WeatherTests
//
//  Created by Karabo Moloi on 2021/08/07.
//

import XCTest
@testable import DVT_Weather

class DVT_WeatherTests: XCTestCase {

    var weatherViewModel = WeatherViewModel()
    
    func testThatRequestGeneratesNonNilURL() {
        let sampleCity = "Pretoria"
        let sampleUnit = "metric"
        let request = weatherViewModel.createWeatherRequest(withCity: sampleCity, unit: sampleUnit)
        let requestURL = request?.url()
        
        XCTAssertNotNil(requestURL)
    }
    
    func testFetchCurrentWeatherFetchesCurrentTemperatureInCelcius() {
        let sampleCity = "Pretoria"
        weatherViewModel.fetchCurrentTemparature(withCity: sampleCity, unit: .celcius, completion: {})
        let currentTemp = weatherViewModel.setTemperatureUnits()
        
        
        XCTAssertEqual(currentTemp.rawValue, "metric")
    }
    
    func testFetchCurrentWeatherFetchesCurrentTemperatureInCelciusFails() {
        let sampleCity = "Pretoria"
        weatherViewModel.fetchCurrentTemparature(withCity: sampleCity, unit: .celcius, completion: {})
        let currentTemp = weatherViewModel.setTemperatureUnits()
        
        
        XCTAssertNotEqual(currentTemp.rawValue, "imperial")
    }
    
    func testFetchCurrentWeatherFetchesCurrentTemperatureInFahrenheit() {
        let sampleCity = "Pretoria"
        weatherViewModel.fetchCurrentTemparature(withCity: sampleCity, unit: .fahrenheit, completion: {})
        let currentTemp = weatherViewModel.setTemperatureUnits()
        
        
        XCTAssertEqual(currentTemp.rawValue, "imperial")
    }
    
    func testFetchCurrentWeatherFetchesCurrentTemperatureInFahrenheitFails() {
        let sampleCity = "Pretoria"
        weatherViewModel.fetchCurrentTemparature(withCity: sampleCity, unit: .fahrenheit, completion: {})
        let currentTemp = weatherViewModel.setTemperatureUnits()
        
        XCTAssertNotEqual(currentTemp.rawValue, "metric")
    }

    func testServiceCallDoesNotSetTemparatureWhenCityParameterEmptyInURL() {
        weatherViewModel.fetchCurrentTemparature(withCity: "", unit: .celcius, completion: {})

        let expected = String()
        let actual = weatherViewModel.currentTemperatureString()

        XCTAssertEqual(expected, actual)
    }
    
    func testServiceCallSetTemparatureWhenCityParameterEmptyInURLFail() {
        weatherViewModel.fetchCurrentTemparature(withCity: "", unit: .celcius, completion: {})

        let sample = "10\u{00B0}C"
        let expected = sample.contains("C")
        let currentWeather = weatherViewModel.currentTemperatureString()
        let actual = currentWeather.contains("C")

        XCTAssertNotEqual(expected, actual)
    }
    
    func testStringContainsWeatherTypeFailsWhenCallNotMade() {
        let type = weatherViewModel.currentTemperatureString()
        var actual = Bool()
        let expected = false
        
        if type.contains("Sunny") || type.contains("Rainy") || type.contains("Cloudy"){
            actual = true
        }
        
        XCTAssertEqual(actual, expected)
    }
    
    func testStringContainsWeatherTypeAfterCallMade() {
        weatherViewModel.fetchCurrentTemparature(withCity: "Pretoria", unit: .celcius, completion: {})
        let type = weatherViewModel.currentTemperatureString()
        var actual = Bool()
        let expected = false
        
        if type.contains("Sunny") || type.contains("Rainy") || type.contains("Cloudy"){
            actual = true
        }
        
        XCTAssertEqual(actual, expected)
    }
    
}
