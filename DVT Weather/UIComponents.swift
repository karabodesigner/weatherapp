//
//  UIComponents.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/14.
//

import Foundation
import UIKit

enum WeatherType {
    case sunny
    case cloudy
    case rainy
}

enum TemperatureUnits: String {
    case celcius = "\u{00B0}C"
    case fahrenheit = "\u{00B0}F"
}
