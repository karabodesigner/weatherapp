//
//  CurrentWeather.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/07.
//

import Foundation

struct Weather: Codable {
    var main: Temperature
    var weather: [WeatherDescription]
}

struct Temperature: Codable {
    var temp: Float
    var temp_min: Float
    var temp_max: Float
}

struct WeatherDescription: Codable {
    var main: String
    var description: String
}

struct Forecast: Codable {
    var list: [ForecastDescription]
}

struct ForecastDescription: Codable {
    var dt_txt: String
    var main: Temperature
    var weather: [WeatherDescription]
}


