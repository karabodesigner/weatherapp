//
//  WeatherViewController.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/09.
//

import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet weak private var backgrounImageView: UIImageView!
    @IBOutlet weak private var minTempLabel: UILabel!
    @IBOutlet weak private var currentTempLabel: UILabel!
    @IBOutlet weak private var maxTempLabel: UILabel!
    @IBOutlet weak private var forecastTableView: UITableView!
    @IBOutlet weak private var largeCurrentTempLabel: UILabel!
    @IBOutlet weak private var containerView: UIView!
    private var forecast = [ForecastDescription]()
    
    private lazy var viewModel = WeatherViewModel()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
//
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupBackground(withColour: viewModel.getBackground())
        setupLabels(withColour: .white)
    }
    
    func setTemparatures() {
        DispatchQueue.main.async {
            self.minTempLabel.text = self.viewModel.minimumTemperatureString()
            self.maxTempLabel.text = self.viewModel.maximumTemperatureString()
            self.currentTempLabel.text = self.viewModel.currentTemperatureString()
            self.largeCurrentTempLabel.text = self.viewModel.currentWeatherType()
        }
    }
    
    func setupLabels(withColour colour: UIColor) {
        self.minTempLabel.textColor = colour
        self.maxTempLabel.textColor = colour
        self.currentTempLabel.textColor = colour
        self.largeCurrentTempLabel.textColor = colour
    }
    
    func setupBackground(withColour colour: UIColor) {
        containerView.backgroundColor = colour
        backgrounImageView.image = viewModel.getBackgroundImage()
        view.backgroundColor = colour
    }
    
    func setupTableView() {
        forecastTableView.dataSource = self
        forecastTableView.delegate = self
        forecastTableView.backgroundColor = viewModel.getBackground()
        forecastTableView.rowHeight = 50
        forecastTableView.separatorStyle = .none
    } 
}

extension WeatherViewController {
    func fetchData() {
        self.viewModel.fetchCity { (city) in
            self.viewModel.fetchCurrentTemparature(withCity: city, unit: .celcius) {
                self.setTemparatures()
            }
            
            self.viewModel.fetchForecast(withCity: city, unit: .celcius, completion: { forecast in
                
                DispatchQueue.main.async {
                    self.forecastTableView.reloadData()
                    self.forecast = self.viewModel.getForecast()
                    self.setupBackground(withColour: self.viewModel.getBackground())
                    self.setupLabels(withColour: .white)
                }
                
            })
        }
    }
}

extension WeatherViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.getForecastCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ForecastCell else {
            return UITableViewCell()
        }
        
        cell.viewModel = viewModel
        cell.temperatureLabel.text = self.viewModel.getForecastTemperature(at: indexPath.row)
        cell.dayLabel.text = self.viewModel.getDayOfTheWeek(at: indexPath.row)
        cell.weatherIcon.image = self.viewModel.setWeatherIcon(at: indexPath.row)
        
        return cell
    }
    
    
}
