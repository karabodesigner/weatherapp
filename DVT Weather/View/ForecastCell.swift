//
//  ForecastCell.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/13.
//

import UIKit

class ForecastCell: UITableViewCell {
    
    private let contentColor: UIColor = .white
    var viewModel: WeatherViewModel? {
        didSet {
            self.backgroundColor = viewModel?.getBackground()
        }
    }
    
    @IBOutlet weak var dayLabel: UILabel! {
        didSet {
            dayLabel.textColor = contentColor
        }
    }
    
    @IBOutlet weak var temperatureLabel: UILabel! {
        didSet {
            temperatureLabel.textColor = contentColor
        }
    }
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
