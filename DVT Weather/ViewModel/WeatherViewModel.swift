//
//  WeatherViewModel.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/09.
//

import Foundation
import CoreLocation
import UIKit

class WeatherViewModel: NSObject {
    
    private var currentTemp: Int?
    private var minTemp: Int?
    private var maxTemp: Int?
    private var currentWeather = String()
    private var locationManager = CityLocationManager()
    private var tempUnit: TempUnits?
    var weatherForecast: [ForecastDescription]?
    
    override init() {
        super.init()
    }
    
//    MARK: - API Call
    func createWeatherRequest(withCity city: String?, unit: String, limit: Int?, endpoint: Endpoint) -> CurrentWeatherRequest? {
        if let city = city {
            return CurrentWeatherRequest(path: endpoint, parameters: queryParameters(withCity: city, unit, limit: limit))
        }
        
        return nil
    }
    
    private func queryParameters(withCity city: String, _ units: String, limit: Int?) -> QueryParameters {
        if limit == nil || limit == 0 {
            let param: QueryParameters = [
                "q": city,
                "appid": APIID.key.rawValue,
                "units" : units
            ]
            
            return param
        } else {
            if let limit = limit {
                let param: QueryParameters = [
                    "q": city,
                    "appid": APIID.key.rawValue,
                    "units" : units,
                    "cnt" : String(limit)
                ]
                
                return param
            }
        }
        
        return QueryParameters()
    }
    
    func fetchCurrentTemparature(withCity city: String, unit: TempUnits, completion: @escaping (() -> Void)) {
        guard let request = createWeatherRequest(withCity: city, unit: unit.rawValue, limit: nil, endpoint: .weather) else {
            return
        }
        self.tempUnit = unit
        
        APICall.loadData(with: request) { (response: Result<Weather?, Error> ) in
            switch response {
            case .failure(let error):
                print("Error: ", error)

            case .success(let result):
                guard let weatherType = result?.weather else {
                    return
                }
                
                guard let temparature = result?.main else {
                    return
                }
                
                self.currentTemp = Int(temparature.temp)
                self.minTemp = Int(temparature.temp_min)
                self.maxTemp = Int(temparature.temp_max)
                
                self.currentWeather = weatherType[0].main
                
                completion()
                
            }
        }
    }
    
    func fetchForecast(withCity city: String, unit: TempUnits, completion: @escaping(([ForecastDescription]) -> Void)) {
        guard let request = createWeatherRequest(withCity: city, unit: unit.rawValue, limit: nil, endpoint: .forecast) else {
            return
        }
        
        self.tempUnit = unit
        
        APICall.loadData(with: request) { (response: Result<Forecast?, Error>) in
            switch response {
            case .failure(let error):
                print("FORECAST Error: \(error)")
                
            case .success(let result):                
                guard let forecast = result?.list else { return }
                self.weatherForecast = forecast
                
                completion(forecast)
            }
        }
    }
    
//    MARK: - Return Functions
    
    func setTemperatureUnits() -> TempUnits {
        return self.tempUnit ?? .celcius
    }
    
    func currentTemperatureString() -> String {
        let currentTempUnit = setTemperatureUnits()
        guard let temp = self.currentTemp else { return "" }
        
        switch currentTempUnit {
        case .celcius:
            return temperatureString(temp, unit: .celcius) + "\nCurrent"
        case .fahrenheit:
            return temperatureString(temp, unit: .fahrenheit) + "\nCurrent"
        }
    }
    
    func minimumTemperatureString() -> String {
        let currentTempUnit = setTemperatureUnits()
        guard let temp = self.minTemp else { return "" }
        
        switch currentTempUnit {
        case .celcius:
            return temperatureString(temp, unit: .celcius) + "\nMin"
        case .fahrenheit:
            return temperatureString(temp, unit: .fahrenheit) + "\nMin"
        }
    }
    
    func maximumTemperatureString() -> String {
        let currentTempUnit = setTemperatureUnits()
        guard let temp = self.maxTemp else { return "" }
        
        switch currentTempUnit {
        case .celcius:
            return temperatureString(temp, unit: .celcius) + "\nMax"
        case .fahrenheit:
            return temperatureString(temp, unit: .fahrenheit) + "\nMax"
        }
    }
    
    private func temperatureString(_ temperature: Int, unit: TemperatureUnits) -> String {
        return "\(temperature)\(unit.rawValue)"
    }
    
    private func getWeatherType(weatherType: WeatherType) -> String {
        let currentTempUnit = setTemperatureUnits()
        var tempUnit: TemperatureUnits
        guard let temp = self.currentTemp else { return "" }
        
        switch currentTempUnit {
        case .celcius:
            tempUnit = .celcius
        case .fahrenheit:
            tempUnit = .fahrenheit
        
        }
        
        switch weatherType {
        case .sunny:
            return "\(self.temperatureString(temp, unit: tempUnit))\nSunny"
        case .cloudy:
             return "\(self.temperatureString(temp, unit: tempUnit))\nCloudy"
        case .rainy:
            return "\(self.temperatureString(temp, unit: tempUnit))\nRainy"
        }
    }
    
    func currentWeatherType() -> String {
        guard let temp = self.currentTemp else { return "" }
        switch self.currentWeather {
        case "Clear":
            return self.getWeatherType(weatherType: .sunny)
        case "Clouds":
            return self.getWeatherType(weatherType: .cloudy)
        case "Rain":
            return self.getWeatherType(weatherType: .rainy)
        default:
            return "\(self.temperatureString(temp, unit: .celcius))\n\(currentWeather)"
        }
    }
    
//    MARK: - User Location(s)
    func fetchCity(_ completion: @escaping((String) -> Void)) {
        locationManager.startUpdatingLocation()
        guard let location = locationManager.cityCoordinates() else {
            return
        }
        
        locationManager.getCurrentCity(location) { (placemark) in
            guard let locality = placemark?.locality else {
                return
            }
            
            completion(locality)
        }
    }
    
//    MARK: - Background
    
    func getBackground() -> UIColor {
        switch self.currentWeather {
        case "Clear":
            return BackgroundColours.sunnyBackground
        case "Clouds":
            return BackgroundColours.cloudyBackground
        case "Rain":
            return BackgroundColours.rainyBackground
        default:
            return UIColor.clear
        }
    }
    
    func getBackgroundImage() -> UIImage {
        switch self.currentWeather {
        case "Clear":
            return BackgroundImage.image(usingName: .sunny)
        case "Clouds":
            return BackgroundImage.image(usingName: .cloudy)
        case "Rain":
            return BackgroundImage.image(usingName: .rainy)
        default:
            return UIImage()
        }
    }
    
//    MARK: - 5 day Forecast
    
    func getForecastCount() -> Int {
        if let forecast = self.weatherForecast?.count {
            return forecast/8
        }
        return [ForecastDescription]().count
    }
    
    func getForecast() -> [ForecastDescription] {
        if let forecast = self.weatherForecast {
            return forecast
        }
        return [ForecastDescription]()
    }
    
    func getForecastTemperature(at index: Int) -> String {
        guard let forecast = self.weatherForecast else {
            return ""
        }
        let unit = setTemperatureUnits()
        var tempCollection = [Int]()
        
        for i in stride(from: 0, to: forecast.count, by: 8) {
            tempCollection.append(Int(forecast[i].main.temp))
        }
        
        switch unit {
        case .celcius:
            return self.temperatureString(tempCollection[index], unit: .celcius)
        case .fahrenheit:
            return self.temperatureString(tempCollection[index], unit: .fahrenheit)
        }
        
    }
    
    func getDayOfTheWeek(at index: Int) -> String {
        guard let forecast = self.weatherForecast else {
            return ""
        }
        var dayCollection = [String]()
        var dateString = String()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        for i in stride(from: 0, to: forecast.count, by: 8) {
            dateString = forecast[i].dt_txt
            if let date = formatter.date(from: dateString) {
                let calandar = Calendar.current
                let components = calandar.component(.weekday, from: date - 1)
                let day = formatter.weekdaySymbols[components]
                dayCollection.append(day)
            }
            
  
        }
        return dayCollection[index]
    }
    
    func setWeatherIcon(at index: Int) -> UIImage {
        guard let forecast = self.weatherForecast else {
            return UIImage()
        }
        var forecastedWeather = [String]()
        
        for i in stride(from: 0, to: forecast.count, by: 8) {
            let weather = forecast[i].weather[0].main
            forecastedWeather.append(weather)
        }
        
        switch forecastedWeather[index] {
        case "Clouds", "Mist", "Haze", "Fog", "Ash", "Dust":
            return WeatherIcon.image(icon: .cloudy)
        case "Sunny":
            return WeatherIcon.image(icon: .sunny)
        case "Rain", "Drizzle", "Thunderstorm":
            return WeatherIcon.image(icon: .rain)
        default:
            return WeatherIcon.image(icon: .sunny)
        }
        
    }

}
