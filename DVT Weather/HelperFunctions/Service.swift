//
//  Service.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/08.
//

import Foundation
import UIKit

protocol Request {
    var path: Endpoint { get }
    var parameters: QueryParameters? { get }
}

extension Request {
    func url() -> URL? {

        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = BaseURL.host.rawValue
        urlComponents.path = BaseURL.path.rawValue + path.rawValue
        urlComponents.queryItems = queryItems
        return urlComponents.url
    }
    
    private var queryItems: [URLQueryItem]? {
        guard let param = parameters else {
            return nil
        }
        
       let queryItem = param.map { (arg0) -> URLQueryItem in
            let (key, value) = arg0
            let val = String(describing: value)
            return URLQueryItem(name: key, value: val)
        }
        
        return queryItem
    }
}

protocol NetworkSession {
    static func loadData<T: Codable>(with request: Request, completion: @escaping(Result<T?, Error>) -> ())
}

struct APICall: NetworkSession {
    static func loadData<T: Codable>(with request: Request, completion: @escaping (Result<T?, Error>) -> ()) {
        
        guard let url = request.url() else {
            return
        }
        
        let session = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                completion(.failure(err))
                return
            }
            
            guard let recievedData = data else {
                return
            }
            
            do {
                let jsonData = try JSONDecoder().decode(T.self, from: recievedData)
                completion(.success(jsonData))
            }catch let error {
                completion(.failure(error))
            }
        }
        session.resume()
    }
}
