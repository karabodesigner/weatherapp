//
//  CityLocationDelegate.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/14.
//

import Foundation
import CoreLocation

class CityLocationManager: NSObject {
    
    private var manager = CLLocationManager()
    
    override init() {
        super.init()
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        self.manager.requestWhenInUseAuthorization()
    }
    
    func startUpdatingLocation() {
        self.manager.startUpdatingLocation()
    }
    
    func cityCoordinates() -> CLLocation? {
        guard let location = self.manager.location else {
            return CLLocation()
        }
        
        let cityCoordinates = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        return cityCoordinates
    }
    
    func stopUpdatingLocation() {
        self.manager.stopUpdatingLocation()
    }
    
    func getCurrentCity(_ location: CLLocation, completion: @escaping(CLPlacemark?) -> ()) {
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if error != nil {
                print("Geocoder error: \(String(describing: error?.localizedDescription))")
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                print("Placemark error: placemark is nil")
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }
    
}
