//
//  ColorExtension.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/14.
//

import Foundation
import UIKit


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let r = CGFloat(red)/255
        let g = CGFloat(green)/255
        let b = CGFloat(blue)/255
        
        self.init(red: r, green: g, blue: b, alpha: 1.0)
    }
}

struct BackgroundColours {
    static let sunnyBackground = UIColor(red: 71, green: 171, blue: 47)
    static let cloudyBackground = UIColor(red: 84, green: 113, blue: 122)
    static let rainyBackground = UIColor(red: 87, green: 87, blue: 93)
}
