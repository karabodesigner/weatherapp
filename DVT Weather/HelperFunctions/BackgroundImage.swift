//
//  BackgroundImage.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/15.
//

import Foundation
import UIKit

struct BackgroundImage {
    static func image(usingName name: BackgroundImageName) -> UIImage {
        guard let image = UIImage(named: "forest_\(name.rawValue)") else {
            return UIImage()
        }
        
        return image
    }
}

enum BackgroundImageName: String {
    case sunny = "sunny"
    case rainy = "rainy"
    case cloudy = "cloudy"
}
