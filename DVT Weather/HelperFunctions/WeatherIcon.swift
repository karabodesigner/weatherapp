//
//  WeatherIcon.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/15.
//

import Foundation
import UIKit

struct WeatherIcon {
    static func image(icon: Icons) -> UIImage {
        guard let image = UIImage(named: "\(icon.rawValue)") else {
            return UIImage()
        }
        
        return image
    }
}

enum Icons: String {
    case sunny = "clear"
    case rain = "rain"
    case cloudy = "partlysunny"
}
