//
//  EnpointComponents.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/09.
//

import Foundation

typealias QueryParameters = [String: Any]

enum APIID: String {
    case key = "79bf8855dc83a9d429baea91e387f95e"
}

enum TempUnits: String {
    case celcius = "metric"
    case fahrenheit = "imperial"
}

enum BaseURL: String {
    case host = "api.openweathermap.org"
    case path = "/data/2.5/"
}

enum Endpoint: String {
    case weather = "weather"
    case forecast = "forecast"
}
