//
//  CurrentWeatherRequest.swift
//  DVT Weather
//
//  Created by Karabo Moloi on 2021/08/09.
//

import Foundation

struct CurrentWeatherRequest: Request {
    var path: Endpoint
    
    var parameters: QueryParameters?
    
    init(path: Endpoint, parameters: QueryParameters) {
        self.path = path
        self.parameters = parameters
    }
    
}
